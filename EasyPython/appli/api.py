from tastypie.resources import ModelResource
from .models import Exercice, Tentative, Progression,ExerciceDansProgression
#from .models import Exercice, Aide, Tentative, Progression,ExerciceDansProgression
from tastypie.authorization import Authorization,DjangoAuthorization, ReadOnlyAuthorization
from tastypie import fields
from tastypie.validation import Validation
import appli.Exercice
from django.contrib.auth.models import User, Group
from tastypie.authentication import BasicAuthentication,SessionAuthentication, Authentication
from tastypie.resources import ModelResource
import json




from tastypie.exceptions import Unauthorized




class ProgressionAuthorization(ReadOnlyAuthorization):
    def create_detail(self, object_list, bundle):
        return bundle.request.user.has_perm("appli.add_progression")


    def update_detail(self, object_list, bundle):
        return bundle.request.user.has_perm("appli.change_progression")
    def update_list(self, object_list, bundle):
        return bundle.request.user.has_perm("appli.change_progression")

    def delete_list(self, object_list, bundle):
        return bundle.request.user.has_perm("appli.delete_progression")

    def delete_detail(self, object_list, bundle):
        return bundle.request.user.has_perm("appli.delete_progression")



class TentativeAuthorization(Authorization):
    def read_list(self, object_list, bundle):
        # This assumes a ``QuerySet`` from ``ModelResource``.
        return object_list

    def read_detail(self, object_list, bundle):
        # Is the requested object owned by the user?
        return True

    def create_list(self, object_list, bundle):
        # Assuming they're auto-assigned to ``user``.
        return object_list

    def create_detail(self, object_list, bundle):
        return bundle.obj.user == bundle.request.user

    def update_list(self, object_list, bundle):
        allowed = []

        # Since they may not all be saved, iterate over them.
        for obj in object_list:
            if obj.user == bundle.request.user:
                allowed.append(obj)

        return allowed

    def update_detail(self, object_list, bundle):
        return True

    def delete_list(self, object_list, bundle):
        # Sorry user, no deletes for you!
        raise Unauthorized("Sorry, no deletes.")

    def delete_detail(self, object_list, bundle):
        raise Unauthorized("Sorry, no deletes.")





class ExerciceAuthorization(DjangoAuthorization):
    def read_detail(self, object_list, bundle):
        return True

    def read_list(self, object_list, bundle):
        return object_list


class GroupResource(ModelResource):
    class Meta:
        queryset = Group.objects.all()
        authentication = SessionAuthentication()
        excludes = ['id']
        include_resource_uri=False


class UserResource(ModelResource):
    groups = fields.ManyToManyField(GroupResource, 'groups', null=True, full=True)
    class Meta:
        queryset = User.objects.all()
        resource_name = 'auth/user'
        excludes = ['email', 'password', 'is_superuser']
        authentication = SessionAuthentication()
        authorization = ReadOnlyAuthorization()


    def get_object_list(self, request):
        return super(UserResource, self).get_object_list(request).filter(id=request.user.id)

    def dehydrate_groups(self, bundle):
        bundle.data["groups"]=list({x.data["name"] for x in bundle.data["groups"]})
        return bundle.data["groups"]

class ProgressionResource(ModelResource):
    class Meta:
        authentication = SessionAuthentication()
      #  authorization = DjangoAuthorization()
        authorization = ProgressionAuthorization()

        queryset = Progression.objects.all()
        resource_name = 'progression'


    def obj_create(self,bundle,**kwargs):
        bundle = super(ProgressionResource,self).obj_create(bundle,**kwargs)
        bundle.obj.exercicedansprogression_set.all().delete()
        e=json.loads(bundle.data["listeExercices"])
        i=0
        for id in e:
            try:
                ex=Exercice.objects.get(pk=id)
                if ex:
                    edp=ExerciceDansProgression.objects.create(exercice=ex, progression=bundle.obj, number=i)
                    edp.save()
                i=i+1
            except Exception as e:
                print(e)

    def dehydrate(self, bundle):
        def estReussi(e):
            return (any(x.reussi for x in Tentative.objects.filter(titre=e, user=bundle.request.user)))
        exos=bundle.obj.get_exercices()
        exos=[{"id":ex.id, "titre":ex.titre,"enonce":ex.enonce, "reussi":estReussi(ex)} for ex in exos]
        bundle.data["listeExercices"]=exos
        return bundle


class ExerciceEnsValidation(Validation):
    def is_valid(self, bundle, request=None):
        if not bundle.data:
            return {'__all__': 'Not quite what I had in mind.'}
        errors = {}
        e=json.loads(bundle.data["resultatsEns"])
        bundle.data["resultats_ens"]=e
        if("erreur" in e):
            return {"erreurs":e["erreur"]}
        if("messagesErreur" in e and len(e["messagesErreur"])>0):
            errors={"erreurs":e["messagesErreur"]}
        return errors



class ExerciceResource(ModelResource):
    class Meta:
        authorization= ExerciceAuthorization()
        authentication = SessionAuthentication()
        queryset = Exercice.objects.all()
        resource_name = 'exercice'
        validation = ExerciceEnsValidation()
        excludes = ['resultatsEns']
        limit = 0
    def dehydrate_moduleEns(self, bundle):
        e=appli.Exercice.ChargerExoEns(bundle.data["moduleEns"])
        return({"nom_solution":e["nom_solution"], "arguments":e["arguments"] })

    def dehydrate(self, bundle):
        resultats=json.loads(bundle.obj.resultatsEns)
        bundle.data["exemples"]=resultats['solutions_visibles']
        return bundle



class ExerciceReussiResource(ModelResource):
    class Meta:
        authorization = ReadOnlyAuthorization()
        authentication = SessionAuthentication()
        queryset=Exercice.objects.all()
        resource_name = 'exerciceReussi'
        excludes = ['resultatsEns']
    def get_object_list(self, request):
        return super(ExerciceReussiResource, self).get_object_list(request).filter(tentative__reussi=True,tentative__user=request.user.id)
    # TODO à changer, passer par les autorisations
    def obj_get_list(self, bundle=None,**kwargs):
        return list(set(self.get_object_list(bundle.request)))



class ResumeExerciceResource(ModelResource):
    class Meta:
        authorization = ExerciceAuthorization()
        authentication=SessionAuthentication()
        queryset = Exercice.objects.all()
        resource_name = 'resumeExercice'
        excludes=["moduleEns"]
        limit=0
    reussi=fields.BooleanField()
    def dehydrate(self, bundle):
        e=Exercice.objects.get(titre=bundle.data["titre"])
        bundle.data["reussi"]=(any(x.reussi for x in Tentative.objects.filter(titre=e, user=bundle.request.user)))
        bundle.data["dansProgression"]=(ExerciceDansProgression.objects.filter(exercice=e).exists())

        return bundle

#class AideResource(ModelResource):
#    class Meta:
#        authorization = Authorization()
#        queryset=Aide.objects.order_by('cle')
#        resource_name='aide'



class GestionExerciceResource(ModelResource):
    class Meta:
        authorization = DjangoAuthorization()
        authentication=SessionAuthentication()
        queryset = Exercice.objects.all()
        resource_name = 'gestion_exercice'
        validation = ExerciceEnsValidation()

    def hydrate_resultatsEns(self,bundle):
        bundle.data["resultatsEns"]=json.dumps(appli.Exercice.TesterExoEns(bundle.data["moduleEns"]))
        return bundle
    def dehydrate_moduleEns(self, bundle):
        return bundle.data["moduleEns"]






class TentativeValidation(Validation):
    def is_valid(self, bundle, request=None):
        if not bundle.data:
            return {'__all__': 'Not quite what I had in mind.'}

        errors = {}
        contenuModuleEns=Exercice.objects.get(titre=bundle.data['titre']).moduleEns
        e=appli.Exercice.TesterSolutionEtu(contenuModuleEns,bundle.data["contenu"],)
        if(("_invalide" in e and e["_invalide"]) or  ("_mauvais_resultats" in e and e["_mauvais_resultats"]) or "erreur" in e ):
            errors=e
        if(errors):
            t=Tentative(user=request.user, titre=Exercice.objects.get(titre=bundle.data['titre']), reussi=False, contenu=bundle.data["contenu"])
            t.save()
        return errors


class TentativeResource(ModelResource):
    user = fields.CharField(attribute='user')
    exercice = fields.CharField(attribute='titre')
    class Meta:
        authorization = TentativeAuthorization() # TODO
        queryset = Tentative.objects.all()
        resource_name = 'tentative'
        validation = TentativeValidation()
        #authentication = SessionAuthentication()
        filtering={
            "exercice":["exact"]
        }

    def hydrate_user(self, bundle):
        bundle.data['user'] = bundle.request.user
        return bundle

    def hydrate_exercice(self, bundle):
        bundle.data['exercice'] = Exercice.objects.get(titre=bundle.data['titre'])
        return bundle


    def get_object_list(self, request):
        return super(TentativeResource, self).get_object_list(request).filter(user=request.user)
         # On ne peut voir que ses propres tentatives

