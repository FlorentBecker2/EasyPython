import inspect
import json,sys,os
import importlib
from io import StringIO
import traceback
import time

# Attention : il faut faire un sandbox !http://www.linux.com/learn/tutorials/382226-run-applications-in-secure-sandboxes-with-selinux
def tente_open(*x):
    raise Interdit("Interdiction d\'utiliser open")
tente_open
op=__builtins__['open']
def toggle_open_off():
        __builtins__['open']=tente_open
def toggle_open_on():
        global op
        __builtins__['open']=op


class Interdit(Exception):
    def __init__(self,chaine):
        pass

class Erreur(Exception):
    def __init__(self,erreur,exception):
        self.erreur = erreur
        self.exception=exception

def remplir_ex(e,pile,limit=None):
    toggle_open_on()
    exc_type, exc_value, exc_traceback = sys.exc_info()
    i=0
    while(exc_traceback.tb_next and i<pile):
        i=i+1
        exc_traceback=exc_traceback.tb_next
    fe=traceback.format_exception(exc_type, exc_value,exc_traceback,limit=limit)
    return (Erreur(str(e), str(''.join(fe))))

def remplir_ex_interdit(e,pile):
    toggle_open_on()
    return remplir_ex(e,pile,limit=1)

def remplir_ex_syn(e):
    toggle_open_on()
    return remplir_ex(e,100,limit=0)

def remplir_ex_import(e):
    toggle_open_on()
    return remplir_ex(e,100,limit=0)
#TODO peut être amélioré pour renvoyer la ligne du fichier où il y a eu un pb d'import


def securiser(f):
    def g(*args,**kwargs):
            toggle_open_off()
            ret=f(*args,**kwargs)
            toggle_open_on()
            return ret
    return g



## Prend en entree un nom de fichier et renvoie le module ouvert du fichier
def ouvre_module(fichier):
    global old_stdout
    (test_fonction.old_stdout,sys.stdout)=(sys.stdout,test_fonction.old_stdout)
    try:
       module=securiser(importlib.import_module)(fichier)
       (test_fonction.old_stdout,sys.stdout)=(sys.stdout,test_fonction.old_stdout)
       return module
    except SyntaxError as e:
       (test_fonction.old_stdout,sys.stdout)=(sys.stdout,test_fonction.old_stdout)
       ex=remplir_ex_syn(e)
    except Interdit as e:
       (test_fonction.old_stdout,sys.stdout)=(sys.stdout,test_fonction.old_stdout)
       ex=remplir_ex_interdit(e,8)
    except Exception as e:
       (test_fonction.old_stdout,sys.stdout)=(sys.stdout,test_fonction.old_stdout)
       ex=remplir_ex_import(e)
    raise ex
## Prend en entrée une fonction, un ensemble d'entrees et renvoie un ensemble de couples (entree,sortie)

def test_fonction(f,entrees):
    def toggle_print():
        global old_stdout
        (test_fonction.old_stdout,sys.stdout)=(sys.stdout,test_fonction.old_stdout)

    fonction=securiser(f)
    pile=traceback.extract_stack()
    try:
        toggle_print()
        res=[]
        for i in entrees:
            res.append((i,fonction(*i)))
        toggle_print()
        return res
    except Interdit as e:
        ex=remplir_ex_interdit(e,len(pile)+2)
    except Exception as e:
        ex=remplir_ex(e,len(pile)+2)
    toggle_print()
    raise ex    

    

test_fonction.old_stdout=open(os.devnull, "w")

