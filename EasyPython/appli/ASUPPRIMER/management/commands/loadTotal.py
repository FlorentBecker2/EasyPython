from django.core.management.base import BaseCommand
import yaml
from django.db import models, migrations
from django.contrib.auth.models import User
from appli.management.commands import loadUser, loadExercice
from django.core.management import call_command



class Command(BaseCommand, migrations.Migration):
	args = '<team_id>'
	help = 'Met a jour la bd entiere en chargeant tous les loads'
	
	def handle(self, *args, **options):
		print ("Please, wait few seconds")
		print (".", end='')
		call_command('loadUser')
		print (".", end='')
		call_command('loadExercice')
		print (".", end='')
		call_command('loadAide')
		print (".", end='')
		call_command('loadTentative')
		print (".", end='')
		call_command('loadTag')
		print (".", end='')
		call_command('loadBadge')
		print (".", end='')
		call_command('loadParcours')
		print (".\nChargement effectué")
