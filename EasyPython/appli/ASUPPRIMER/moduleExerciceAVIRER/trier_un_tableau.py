def solution(fun):
    fun.solution=True
    return fun

entrees_visibles=[
        ([5,5]),
        ([5,3]),
        ([3,5]),
]
entrees_invisibles=[
([4,7]),
([5,9]),
([10,8]),
]


@solution
def tri(a):
    return a.sort()
