from django.contrib import admin
from .models import Utilisateur, Exercice, Tentative, Progression, ExerciceDansProgression

# Register your models here.

admin.site.register(Utilisateur)
#admin.site.register(Badge)
admin.site.register(Exercice)

#admin.site.register(Tag)

admin.site.register(Tentative)
admin.site.register(Progression)
admin.site.register(ExerciceDansProgression)
