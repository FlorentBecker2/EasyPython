# -*- coding: utf-8 -*-
from django.db import models, migrations, transaction
import json

def creer_liste(apps, schema_editor):
    # We can't import the Person model directly as it may be a newer
    # version than this migration expects. We use the historical version.
    Progression = apps.get_model("appli", "Progression")
    Exercice= apps.get_model("appli", "Exercice")
    ExerciceDansProgression= apps.get_model("appli", "ExerciceDansProgression")
    for progression in Progression.objects.all():
        e=json.loads(progression.listeExercices)
        i=0;
        for id in e:
            try:
                ex=Exercice.objects.get(pk=id)
                if ex:
                    print(ex)
                    with transaction.atomic():
                        edp=ExerciceDansProgression.objects.create(exercice=ex, progression=progression, number=i)
                        edp.save()
                i=i+1
            except Exception as e:
                print(e)

class Migration(migrations.Migration):

    dependencies = [
        ('appli', '0006_auto_20151014_1142'),
    ]

    operations = [
        migrations.RunPython(creer_liste),
    ]
