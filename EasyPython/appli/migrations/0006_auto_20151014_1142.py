# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('appli', '0005_auto_20151012_0841'),
    ]

    operations = [
        migrations.CreateModel(
            name='ExerciceDansProgression',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('number', models.PositiveIntegerField()),
                ('exercice', models.ForeignKey(to='appli.Exercice')),
                ('progression', models.ForeignKey(to='appli.Progression')),
            ],
            options={
                'ordering': ('number',),
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='progression',
            name='exercices',
            field=models.ManyToManyField(through='appli.ExerciceDansProgression', to='appli.Exercice'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='tentative',
            name='reussi',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
    ]
