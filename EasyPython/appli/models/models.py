from django.db import models
from django.contrib.auth.models import User
import json
import os
import tempfile
import time
# =======================================================================

class Exercice(models.Model):
    titre = models.CharField(max_length=200, unique=True)
    auteur = models.CharField(max_length=200)
    enonce = models.CharField(max_length=200)
    date = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name="Date de parution")
    #tags = models.ManyToManyField(Tag)
    moduleEns = models.TextField()
    resultatsEns=models.TextField()
    commentaires = models.TextField()
    def __str__(self):
        return self.titre

    def get_date(dt):
        return dt.strftime("%Y-%M-%d")

class Progression(models.Model):
    titre=models.CharField(max_length=200, unique=True)
    sujet = models.TextField()
#    listeExercices = models.TextField()
    exercices=models.ManyToManyField('Exercice', through='ExerciceDansProgression')
    def get_exercices(self):
            return self.exercices.order_by('exercicedansprogression')



class ExerciceDansProgression(models.Model):
    exercice = models.ForeignKey(Exercice)
    progression = models.ForeignKey(Progression)
    number = models.PositiveIntegerField()
    class Meta:
       ordering = ('number',)



#class Aide(models.Model):
#    cle = models.CharField(max_length=200, unique=True, primary_key=True)
#    definition = models.CharField(max_length=200)
#
#    def __str__(self):
#        return "( cle=%s, definition=%s )" % (self.cle, self.definition)
#
#
# class Article(models.Model):
#     nom = models.CharField(max_length=200)
#     date = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name="Date de parution")
#     lien = models.CharField(max_length=200)
#     affichage = models.CharField(max_length=200)
#
#     def get_date(dt):
#         dt.strftime("%Y-%M-%d")
#

#class Badge(models.Model):
#    nom = models.CharField(max_length=200, primary_key=True)
#    image = models.CharField(max_length=200)
#    description = models.CharField(max_length=200)
#
#    def __str__(self):
#        return "(nom=%s, description=%s, image=%s)" % (self.nom, self.description, self.image)


class Utilisateur(models.Model):
    user = models.OneToOneField(User)
    groupe = models.IntegerField(null=False)
#    badges = models.ManyToManyField(Badge)

    def __str__(self):
        return "( prenom=%s, nom=%s, mdp=%s, groupe=%s)" % (
            self.user.first_name, self.user.last_name, self.user.password, self.groupe)


# Classes de relation
class Tentative(models.Model):
    user = models.ForeignKey(User)
    titre = models.ForeignKey('Exercice')
    reussi = models.BooleanField(default=True)
    date = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name="Date de la tentative")
    contenu = models.TextField()
    def __str__(self):
        return "(nom eleve=%s, titre=%s, date=%s, reussi=%s)" % (self.user, self.titre, self.date, self.reussi)

    def get_date(dt):
        dt.strftime("%Y-%M-%d")


#class Etape(models.Model):
#    titre = models.CharField(max_length=200, primary_key=True)
#    nb = models.IntegerField(null=1)
#    exercices = models.ManyToManyField(Exercice)
#
#    def __str__(self):
#        return "(titre=%s, nb d'ex attendu=%s)" % (self.titre, self.nb)


#class OrdreEtape(models.Model):
#    etape = models.ForeignKey('Etape')
#    num = models.IntegerField(null=False, unique=True)
#
#    def __str__(self):
#        return "(etape=%s, numero=%s)" % (self.etape, self.num)


#class Lot(models.Model):
#    titre = models.CharField(max_length=200, primary_key=True)
#    etapes = models.ManyToManyField(OrdreEtape)

#    def __str__(self):
#        return "(titre=%s)" % (self.titre)

#
# # =======================================================================
# # requetes
#
# # Pour ajouter des instances
# def aj_util(p, n, passw, g, t):
#     new_util = Utilisateur(prenom=p, nom=n, password=passw, groupe=g, type=t)
#     new_util.save()
#     return True
#
#
# def aj_art(n, l, a):
#     new_art = Article(nom=n, lien=l, affichage=a)
#     new_art.save()
#     return True
#
#
# def aj_exo(t, a, e):
#     new_ex = Exercice(titre=t.lower(), auteur=a, enonce=e)
#     new_ex.save()
#     return True
#
#
# def aj_aide(mot, aide):
#     print("122 " + str(len(get_aide(mot))))
#     if len(get_aide(mot)) == 0:
#         print("123 mod if")
#         new_help = Aide(cle=mot.lower(), definition=aide)
#         new_help.save()
#         return True
#     return False
#
#
# # concernant les tentatives
# def aj_tentative(nom_util, t):  # Le nom_util est le nom de l'utilisateur
#     user = get_utilisateur(nom_util)  # et t est l'ID de l'exercice
#     exercice = get_exo(t)
#     new_tentative = Tentative.objects.create(nom=user, titre=exercice)
#     new_tentative.save()
#     return (True, new_tentative)
#
#
# def aj_tentative_2(nom_util, t, b):  # Le nom_util est le nom de l'utilisateur
#     user = get_utilisateur(nom_util)  # et t est l'ID de l'exercice
#     exercice = get_exo(t)
#     new_tentative = Tentative.objects.create(nom=user, titre=exercice, reussi=b)
#     new_tentative.save()
#     return (True, new_tentative)
#
#
# def aj_tag(nom):
#     new_tag = Tag(nom=nom)
#     new_tag.save()
#     return True
#
#
# # concernant les badges
# def aj_badge_user(user, badge):  # Pour ajouter un badge a un utilisateur
#     user.utilisateur.badges.add(badge)
#     return True
#
#
# def verif_badge_user(nom_util):  # Pour vérifier les badges que l'utilisateur a obtenu
#     if (len(nb_reussite(nom_util)) >= 10 and len(get_utilisateur(nom_util).badges.all().filter(nom="ten")) == 0):
#         aj_badge_user(nom_util, get_badge("ten"))
#     # ~ TEST
#     # ~ aj_badge_user(nom_util, get_badge("ten"))
#     # ~ aj_badge_user(nom_util, get_badge("papierpeint"))
#     # ~ aj_badge_user(nom_util, get_badge("connecte"))
#     # ~ aj_badge_user(nom_util, get_badge("help"))
#     # ~ aj_badge_user(nom_util, get_badge("economy"))
#     # ~ aj_badge_user(nom_util, get_badge("beta"))
#     # ~ /TEST
#     if (len(nb_reussite(nom_util)) >= 1 and len(get_utilisateur(nom_util).badges.all().filter(nom="one")) == 0):
#         aj_badge_user(nom_util, get_badge("one"))
#     return True
#
#
# # concernant les parcours (lots)
# def aj_etape(nom,
#              nbRepAttendues):  # nbRepAttendues est le nb d'exercices que l'utilisateur doit reussir pour passer à la suite
#     new_etape = Etape(titre=nom, nb=nbRepAttendues)
#     new_etape.save()
#     return True

#
# ######
# # concernant les parcours (lots)
# def aj_etape(nom,
#              nbRepAttendues):  # nbRepAttendues est le nb d'exercices que l'utilisateur doit reussir pour passer à la suite
#     new_etape = Etape(titre=nom, nb=nbRepAttendues)
#     new_etape.save()
#     return True
#
#
# def aj_ordre_etape(step, n):
#     new_ordre = OrdreEtape(etape=get_etape(step), num=n)
#     new_ordre.save()
#     return True
#
#
# def aj_lot(t):
#     new_lot = Lot(titre=t)
#     new_lot.save()
#     return True
#
#
# def aj_ordreEtape_Lot(t, l):
#     ordre = get_ordreEtape(t)
#     lot = get_lot(l).etapes.add(ordre)
#     return True
#
#
# ######
# # Pour recuperer des instances
# def get_tentative():
#     return Tentative.objects.all()
#
#
# # concernant les exercices
# def get_exos():
#     return Exercice.objects.order_by('-date')
#
#
# def get_last_exo():
#     t=Exercice.objects.order_by('-date')
#     if len(t)>0:
#         return t[0]
#     else:
#         return None
#
#
#
#
# def get_exo_titre(t):
#     if len(Exercice.objects.filter(titre=t)) != 0:
#         return Exercice.objects.filter(titre=t)[0]
#     return False
#
#
# # concernant les utilisateurs
# def get_utils():
#     return Utilisateur.objects.all()
#

# avoir les utilisateurs par groupe
# def get_utils(gp):
#     if gp == 1:
#         return Utilisateur.objects.all()
#     elif gp == 11:
#         aux = (Utilisateur.objects.filter(groupe=111).all() | Utilisateur.objects.filter(groupe=112).all())
#         # ~ aux.append(Utilisateur.objects.filter(groupe=112).all())
#         return aux
#     elif gp == 12:
#         aux = (Utilisateur.objects.filter(groupe=121).all() | Utilisateur.objects.filter(groupe=122).all())
#         # ~ aux.append(Utilisateur.objects.filter(groupe=122).all())
#         return aux
#     elif gp == 13:
#         aux = (Utilisateur.objects.filter(groupe=131).all() | Utilisateur.objects.filter(groupe=132).all())
#         # ~ aux.append(Utilisateur.objects.filter(groupe=132).all())
#         return aux
#     elif gp == 14:
#         aux = (Utilisateur.objects.filter(groupe=141).all() | Utilisateur.objects.filter(groupe=142).all())
#         # ~ aux.append(Utilisateur.objects.filter(groupe=142).all())
#         return aux
#     else:
#         return Utilisateur.objects.filter(groupe=gp).all()
#
#
# def get_utilisateur(n):
#     if len(User.objects.filter(last_name=n)) != 0:
#         u = User.objects.filter(last_name=n)
#         return Utilisateur.objects.filter(user=u)[0]
#     return False


# concernant les tags
# def get_tags():
#     return Tag.objects.order_by('nom')
#
#
# def get_tag_par_nom(n):
#     liste = Tag.objects.filter(nom=n)
#     if len(liste) != 0:
#         return liste[0]
#     return False
#
#
# def get_tag_exercice(nom_ex):
#     return get_exo_titre(nom_ex).tags.all()
#
#
# # concernant les aides
# def get_aide(mot):
#     return Aide.objects.filter(cle=mot.lower())
#
#
# def get_aides():
#     return Aide.objects.order_by('cle')
#
#
# def recherche(nom, tags):
#     exos = set()
#     print(type(nom))
#     print(type(tags))
#     if (len(nom) == 0 and len(tags) == 0):
#         return Exercice.objects.all()
#     if (len(nom) != 0 and len(tags) != 0):
#         for t in tags:
#             exos_tag = Exercice.objects.filter(tags=t)
#             for e in exos_tag:
#                 exos.add(e)
#         exos_nom = Exercice.objects.filter(titre__contains=nom)
#         for e in exos_nom:
#             exos.add(e)
#         return (exos)
#     if (len(nom) == 0 and len(tags) != 0):
#         for t in tags:
#             exos_tag = Exercice.objects.filter(tags=t)
#             for e in exos_tag:
#                 exos.add(e)
#         return (exos)
#     if (len(nom) == 0 and len(list_tags) == 0):
#         exos.add(Exercice.objects.filter(titre__contains=nom))
#         return (exos)
#
#
# # concernant les badges
# def get_badges():  # Recuperer tous les badges
#     return Badge.objects.all()
#
#
# def get_badge(name):  # Recuperer le badge de nom name
#     return Badge.objects.filter(nom=name).all()[0]
#
#
# def get_badges_Utilisateur(user):  # Recuperer les badges d'un utilisateur
#     return user.utilisateur.badges.all()
#
#
# def nb_reussite(nom2):  # Recuperer toutes les reussites
#     return Tentative.objects.filter(nom=get_utilisateur(nom2)).filter(reussi=True).all()
#
#
# # concernant les graphes
# def nb_tentative(nom2, titre2):
#     if (titre2 == None):  # si il n'y a pas d'exercice selectionné, on veut le nombre total d'exercice tenté
#         return Tentative.objects.filter(nom=get_utilisateur(nom2)).all()
#     else:
#         return Tentative.objects.filter(nom=get_utilisateur(nom2)).filter(titre=get_exo_titre(titre2)).all()
#
#
# def nb_tentative_reussite(nom2, titre2):
#     if (titre2 != None):
#         return Tentative.objects.filter(nom=get_utilisateur(nom2)).filter(titre=get_exo_titre(titre2)).filter(
#             reussi=True).all()
#     else:  # si il n'y a pas d'exercice selectionné, on récupère toutes les tentatives réussites
#         return Tentative.objects.filter(nom=get_utilisateur(nom2)).filter(reussi=True).all()
#
#
#     # pour avoir le nb de tentative reussie par groupe
#
#
# def nb_tentative_reussite_groupe(groupe, exercice):
#     maliste = []
#
#     for user in groupe:
#         maliste.append(nb_tentative_reussite(user.user.last_name, exercice))
#
#     maListeFinale = []
#     for liste in maliste:
#         for elem in liste:
#             maListeFinale.append(elem)
#
#     return maListeFinale
#
#
# def get_util_groupe(groupe):
#     return (Utilisateur.objects.filter(groupe=groupe))
#
#
# # La nb de tentative par groupe
# def nb_tentatives_groupes(groupe, exercice):
#     maliste = []
#     print("groupe :")
#     print(groupe)
#
#     for user in groupe:
#         maliste.append(nb_tentative(user.user.last_name, exercice))
#
#     maListeFinale = []
#     for liste in maliste:
#         for elem in liste:
#             maListeFinale.append(elem)
#
#     return maListeFinale
#
#
# # concernant les articles
# def get_arts():
#     return Article.objects.order_by('-date')
#
#
# # concernant les lots
# def get_etape(t):  # acces par titre
#     return Etape.objects.filter(titre=t).all()[0]
#
#
# def get_ordreEtape(t):
#     return OrdreEtape.objects.filter(etape=get_etape(t))[0]
#
#
# def get_lots():  # tous les lots
#     return Lot.objects.all()
#
#
# def get_lot(t):  # acces par titre
#     return Lot.objects.filter(titre=t).all()[0]
#
#
# def get_ordreEtapeParLot(lot):
#     return Lot.objects.filter(titre=lot)[0].etapes.all()
#
#
# # =======================================================================
# # Fonctions utiles au views
# # Module Exercice
# def exoEns(n, c):
#     print("TODO DOIT ETRE SUPPRIME")
#     dir_path = os.path.dirname(os.path.abspath(
#         __file__)) + "/moduleExercice"  # /!\ Cela sert à connaitre le chemin absolu jusqu'au dossier reponses (la ou sont stockees toutes les tentatives /!\
#     print ("ijnp" + dir_path)
#     print (n + ".py")
#     print(os.system("cp " + n + ".py" + " " + dir_path))
#     os.system("cp " + n + ".py" + " " + dir_path)
#     os.system("cd" + " " + dir_path)
#     exoEns = open(n + ".py", 'w')
#     print("debug")
#
#     exoEns.write("def solution(fun):" + "\n" +
#                  "	" + "fun.solution=True" + "\n" +
#                  "	" + "return fun" + "\n" +
#                  "\n" +
#                  c)
#
#     dir_path = os.path.dirname(os.path.abspath(
#         __file__)) + "/moduleExercice"  # /!\ Cela sert à connaitre le chemin absolu jusqu'au dossier reponses (la ou sont stockees toutes les tentatives /!\
#     print (dir_path)
#
#     print(exoEns)
#     exoEns.close()
#     os.system("cp " + n + ".py" + " " + dir_path)
#     os.system("rm " + n + ".py")
#
#     os.system("cd appli/moduleExercice" + "\n" +  # On se met a l'endroit du module pour tester les exercices
#               "chmod u+x " + n + ".py")
#
#
# def ecrireEnteteForm():
#     string2 = "#Les entrées sont les paramètres de la fonction. Merci de les inscrire entre les parenthèses.\n" + "entrees_visibles=[\n" + "	(),\n" + "	()\n" + "\n" + "]" + "\n" + "\n\nentrees_invisibles=[\n" + "	(),\n" + "	()\n" + "\n" + "]\n\n" + "#Veuillez entre le code apres le @solution" + "\n" + "@solution" + "\n" + "def \n" + "\n" + "\n"
#     return string2
#
#
# def ecrireEnteteCode(nom_solution, arguments):
#     string = "def " + nom_solution + "("
#     for arg in arguments[:-1]:
#         string += arg + ", "
#     string += arguments[-1] + "):\n	return None"
#
#     return string

#
# # Cette fct est aussi bien utilisée par les étudiants que pour vérifier les exercices de l'enseignant
# def testerSolution(reponse, titre):
#     print (titre)
#     creaFichier = tempfile.NamedTemporaryFile()  # On va mettre la reponse de l'etudiant dans un fichier pour tester
#     print (creaFichier.name)  # print pour connaitre le nom du fichier cree
#     Fichier = open(creaFichier.name, 'w')
#     Fichier.write(reponse)
#     Fichier.close()
#     dir_path = os.path.dirname(os.path.abspath(
#         __file__)) + "/moduleExercice"  # /!\ Cela sert à connaitre le chemin absolu jusqu'au dossier reponses (la ou sont stockees toutes les tentatives /!\
#     print (dir_path)  # print pour verifier que le chemin est correct
#     print ("DIR=" + os.path.dirname(os.path.abspath(titre + ".py")))
#     print (titre)
#     os.system("cd appli/moduleExercice" + "\n" +  # On se met a l'endroit du module pour tester les exercices
#               "cp " + creaFichier.name + " " + dir_path + "\n" +  # On copie le fichier temporaire pour pouvoir le tester
#               "mv " + creaFichier.name[5:] + " " + creaFichier.name[5:] + ".py" + "\n" +
#               "chmod u+x " + creaFichier.name[5:] + ".py" + "\n" +
#               "python3 main.py " + titre + " " + creaFichier.name[5:] + " > ../../Reponses/" + creaFichier.name[
#                                                                                                5:] + ".json")  # On a tout prepare, on peut alors verifier la solution
#
#     time.sleep(1)
#
#     Fichier2 = open("./Reponses/" + creaFichier.name[5:] + ".json", "r")
#     r = Fichier2.read().split("\n")[-2]
#     print(r)
#     Fichier2.close()
#     Fichier3 = open("./Reponses/" + creaFichier.name[5:] + ".json", "w")
#     Fichier3.write(r)
#     Fichier3.close()
#     exercices = json.load(open("./Reponses/" + creaFichier.name[5:] + ".json"))
#     mauvais_resultats = exercices["_mauvais_resultats"]
#     invalide = exercices["_invalide"]
#
#     booleen = False
#     destroy = False
#     if (mauvais_resultats == [[], []]):
#         # ~ print ("456: booleen= True")
#         booleen = True
#
#     elif (mauvais_resultats == []):
#         destroy = True
#
#     elif mauvais_resultats != [[], []]:
#         print (mauvais_resultats)
#
#     time.sleep(2)
#
#     os.system("cd appli/moduleExercice" + "\n" +  # On revient dans le dossier Reponses
#               "rm " + creaFichier.name[5:] + ".py")  # et on supprime le fichier qui est devenu inutile.
#     os.system("cd Reponses" + "\n" +  # On revient dans le dossier Reponses
#               "rm " + creaFichier.name[5:] + ".json")
#
#     return (mauvais_resultats, booleen, destroy, invalide)
# #
#
# # Badge
# # Cultive
# def cultive(user):
#     if len(user.utilisateur.badges.all().filter(nom="cultivé")) == 0:
#         aj_badge_user(user, "cultivé")
#         return True
#     return False
#
#
# def beta(user):
#     if len(user.utilisateur.badges.all().filter(nom="beta")) == 0:
#         aj_badge_user(user, "beta")
#         return True
#     return False
#
#
# def help(user):
#     if len(user.utilisateur.badges.all().filter(nom="help")) == 0:
#         aj_badge_user(user, "help")
#         return True
#     return False
#
#
# def demolisseur(user):
#     if len(user.utilisateur.badges.all().filter(nom="demolisseur")) == 0:
#         aj_badge_user(user, "demolisseur")
#         return True
#     return False
