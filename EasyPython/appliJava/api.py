from tastypie.resources import ModelResource
from .models import Exercice, Aide, Tentative, Progression,ExerciceDansProgression
from tastypie.authorization import Authorization,DjangoAuthorization, ReadOnlyAuthorization
from tastypie import fields
from tastypie.validation import Validation
import appli.Exercice
from django.contrib.auth.models import User, Group
from tastypie.authentication import BasicAuthentication,SessionAuthentication, Authentication
from tastypie.resources import ModelResource
import json




from tastypie.exceptions import Unauthorized

class GroupResource(ModelResource):
    class Meta:
        queryset = Group.objects.all()
        authentication = SessionAuthentication()
        excludes = ['id']
        include_resource_uri=False


class UserResource(ModelResource):
    groups = fields.ManyToManyField(GroupResource, 'groups', null=True, full=True)
    class Meta:
        queryset = User.objects.all()
        resource_name = 'auth/user'
        excludes = ['email', 'password', 'is_superuser']
        authentication = SessionAuthentication()
        authorization = DjangoAuthorization()


    def get_object_list(self, request):
        return super(UserResource, self).get_object_list(request).filter(id=request.user.id)

    def dehydrate_groups(self, bundle):
        bundle.data["groups"]=list({x.data["name"] for x in bundle.data["groups"]})
        return bundle.data["groups"]

class ExerciceEnsValidation(Validation):
    def is_valid(self, bundle, request=None):
        if not bundle.data:
            return {'__all__': 'Not quite what I had in mind.'}
        errors = {}
        e=json.loads(bundle.data["resultatsEns"])
        bundle.data["resultats_ens"]=e
        if("messagesErreur" in e and len(e["messagesErreur"])>0):
            errors={"erreurs":e["messagesErreur"]}
        return errors



class ExerciceResource(ModelResource):
    class Meta:
        authorization = DjangoAuthorization()
        authentication = SessionAuthentication()
        queryset = Exercice.objects.all()
        resource_name = 'exercice'
        validation = ExerciceEnsValidation()
        excludes = ['resultatsEns']

    def dehydrate_moduleEns(self, bundle):
        e=appli.Exercice.ChargerExoEns(bundle.data["moduleEns"])
        return({"nom_solution":e["nom_solution"], "arguments":e["arguments"] })

    def dehydrate(self, bundle):
        resultats=json.loads(bundle.obj.resultatsEns)
        bundle.data["exemples"]=resultats['solutions_visibles']
        return bundle
