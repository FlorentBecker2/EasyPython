#!/bin/bash

# $1 : java à tester
# $2 : java unit test
classpath=/Applications/PyCharm.app/lib/junit.jar:.
mkdir /tmp/testEns
rm -rf /tmp/testEns/*

cp $1 $2 /tmp/testEns
echo cd /tmp/testEns
cd /tmp/testEns
echo javac -cp $classpath  $1
javac -cp $classpath  $1
echo javac -cp $classpath $1
javac -cp $classpath $2

java -cp $classpath junit.textui.TestRunner $(basename $2 .java)

#javac -cp /Applications/PyCharm.app/lib/junit.jar:.  MonTesteur.java
#java -cp /Applications/PyCharm.app/lib/junit.jar:. MonTesteur


