var React = require('react');
var $ = require('jquery');
var mui = require('material-ui');

var Router = require('react-router');
var RouteHandler = Router.RouteHandler;
var AppLeftNav = require('./AppLeftNav.jsx'); // Our custom react component
var Login = require('./Login.jsx'); // Our custom react component
var Notifications = require('./Notifications.jsx'); // Our custom react component
var Typography = mui.Styles.Typography;
const ThemeManager = mui.Styles.ThemeManager;
const DefaultRawTheme = mui.Styles.LightRawTheme;

var { AppBar, AppCanvas, Menu, IconButton, Styles, FlatButton, IconButton } = mui;
var { Spacing, Colors } = Styles;

var Principal=React.createClass({
  contextTypes : {
        router: React.PropTypes.func
    },
  childContextTypes : {
    muiTheme: React.PropTypes.object,
    notification: React.PropTypes.func.isRequired
      },
  getInitialState() {
      if(localStorage.getItem("user")!=null)
         return {
          sousTitre:"",
          loggedIn:true
          };
      else
         return {
          sousTitre:"",
          loggedIn:false
          };
       },

    showNotification(chaine)
    {
        this.refs.notification.show(chaine)
    },


  getChildContext() {
    return {
     muiTheme: ThemeManager.getMuiTheme(DefaultRawTheme),
     notification: this.showNotification
    }
  },

  getStyles() {
    var darkWhite = Colors.darkWhite;
    return {
      footer: {
        backgroundColor: Colors.grey900,
        textAlign: 'center'
      },
      a: {
        color: darkWhite
      },
      p: {
        margin: '0 auto',
        padding: '0',
        color: Colors.lightWhite,
        maxWidth: '335px'
      },
      iconButton: {
        color: darkWhite
      },
      root: {
        paddingTop: Spacing.desktopKeylineIncrement + 'px'
        //paddingTop: '23px'

      },

      content: {
        boxSizing: 'border-box',
        //padding: Spacing.desktopGutter + 'px',
        paddingTop:"0"
        //maxWidth: (Spacing.desktopKeylineIncrement * 14) + 'px'
      }
    };
  },

  render() {
    var styles = this.getStyles();
    var title =
      this.context.router.isActive('exercice') ? 'Résolution Exercice' :
      this.context.router.isActive('liste-exercices') ? 'Exercices' :
      this.context.router.isActive('gerer_exercice') ? "Gestion d'exercice" :
      this.context.router.isActive('progressions') ? "Sujets" :
      this.context.router.isActive('exerciceProgression') ? ('Exercice '+ this.state.sousTitre):
      this.context.router.isActive('profil') ? "Profil" :
      this.context.router.isActive('progression') ? this.state.sousTitre :

      'Accueil';

    var boutonLogin;

    if(!this.state.loggedIn)
      boutonLogin=
      <IconButton
       onClick={this.login.bind(this)}
       tooltip="Connecter"
       >
          <i className="material-icons" >input</i>
      </IconButton>
    else
    boutonLogin=
       <IconButton
        onClick={()=>{
        $.get("deconnexion")
        localStorage.removeItem("user");
          this.setState({loggedIn:false})} }
        tooltip="Deconnecter">
                <i className="material-icons" >launch</i>
          </IconButton>

    return (
      <AppCanvas>
        <AppBar
            style={{
                paddingLeft:'10%'
            }}
          onLeftIconButtonTouchTap={this._onLeftIconButtonTouchTap}
          title={title}
          zDepth={0}
          iconElementRight={boutonLogin}
          />
	     <Notifications ref="notification"/>

        <AppLeftNav ref="leftNav" />
        <div style={styles.root}>
         <div style={styles.content}>
         <RouteHandler style={{
            marginLeft:'10%',
            marginRight:'10%',
            }}
            changeSousTitre={((x)=>this.setState({sousTitre:x})).bind(this)}
         />
         </div>
       </div>
       <Login ref="login" onLogin={()=>this.setState({loggedIn:true})} />
      </AppCanvas>

    );
  },

  login(){
    this.refs.login.show();
  },

  _onLeftIconButtonTouchTap() {
    this.refs.leftNav.toggle();
  }
});



module.exports = Principal;
