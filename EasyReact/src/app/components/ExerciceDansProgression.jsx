let React = require('react');
let Router = require('react-router');
let RouteHandler = Router.RouteHandler;
let { Menu, Mixins, Styles, MenuItem} = require('material-ui');
var $ = require('jquery');

let { Spacing, Colors } = Styles;
let { StyleResizable, StylePropable } = Mixins;

let ExerciceDansProgression = React.createClass({

  mixins: [StyleResizable, StylePropable],

  contextTypes: {
    router: React.PropTypes.func
  },

  propTypes: {
    progression: React.PropTypes.number.isRequired,
  },



  getInitialState: function() {
        return {
            menuItems:[],
        };
  },
  getDefaultProps: function() {},
  componentWillMount : function() {},
  componentWillReceiveProps: function() {},
  componentWillUnmount : function() {},
  _parseData : function() {},
  _onSelect : function() {},

 componentDidMount: function() {
        var path=this.context.router.getCurrentParams()

        if(path.progression && path.progression != 0 )
           {
            $.get("api/v1/progression/"+path.progression+"/?format=json", function(result) {
                var menuItems=[];
                menuItems.push( { text: result.titre,route:"progression",
                        params: {"id":path.progression}, })
                menuItems.push( { type: MenuItem.Types.SUBHEADER,text: "" })


                for(var i in result.listeExercices){
                    menuItems.push( {
                        route:"exerciceProgression",
                        params: {"progression":path.progression,
                                  "id":result.listeExercices[i].id},
                        text: result.listeExercices[i].titre + (result.listeExercices[i].reussi?"  *":"")
                                    })
                }
                this.setState({menuItems:menuItems});
            }.bind(this)
         )
         }
  },






  getStyles(){
    let subNavWidth = Spacing.desktopKeylineIncrement * 3 + 'px';
    let styles = {
      root: {
        //paddingTop: Spacing.desktopKeylineIncrement + 'px'
      },
      rootWhenMedium: {
        position: 'relative'
      },
      secondaryNav: {
        borderTop: 'solid 1px ' + Colors.grey300,
        overflow: 'hidden'
      },
      content: {
        boxSizing: 'border-box',
        padding: Spacing.desktopGutter + 'px',
        maxWidth: (Spacing.desktopKeylineIncrement * 14) + 'px'
      },
      secondaryNavWhenMedium: {
        borderTop: 'none',
        position: 'absolute',
        top: '64px',
        width: subNavWidth
      },
      contentWhenMedium: {
        marginLeft: subNavWidth,
        borderLeft: 'solid 1px ' + Colors.grey300,
        minHeight: '800px'
      }
    };

    if (this.isDeviceSize(StyleResizable.statics.Sizes.MEDIUM) ||
        this.isDeviceSize(StyleResizable.statics.Sizes.LARGE)) {
      styles.root = this.mergeStyles(styles.root, styles.rootWhenMedium);
      styles.secondaryNav = this.mergeStyles(styles.secondaryNav, styles.secondaryNavWhenMedium);
      styles.content = this.mergeStyles(styles.content, styles.contentWhenMedium);
    }

    return styles;
  },

  render() {
    let styles = this.getStyles();
    var path=this.context.router.getCurrentParams()

      let {style, ...other,} = this.props;

    return (
      <div style={styles.root}>
        <div style={styles.content}>
            <RouteHandler {...other}/>
        </div>
        <div style={styles.secondaryNav}>
          <Menu
            ref="menuItems"
            zDepth={0}
            menuItems={this.state.menuItems}
            selectedIndex={this._getSelectedIndex()}
            onItemTap={this._onMenuItemClick}
            menuItemStyle={{lineHeight:"100%", marginBottom:"10px"}}
             />
        </div>
      </div>
    );
  },

  _getSelectedIndex() {
    let menuItems = this.state.menuItems;
    let currentItem;

    for (let i = menuItems.length - 1; i >= 0; i--) {
      currentItem = menuItems[i];
      if (currentItem.route && this.context.router.isActive(currentItem.route)&& (!currentItem.params || this.context.router.isActive(currentItem.route,currentItem.params)))
       return i;
    }
  },

  _onMenuItemClick(e, index, item) {
    this.context.router.transitionTo(item.route, item.params);
  }

});




module.exports = ExerciceDansProgression;
