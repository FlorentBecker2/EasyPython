var $ = require('jquery');
let React = require('react');
var mui = require('material-ui');
var Exercice = require("./Exercice.jsx")
//var EditeExercice = require("./EditeExercice.jsx")
//var FaireExercice = require("./FaireExercice.jsx")
var Auth=require('../policies/Auth.jsx')
var Router = require('react-router');
var Link=Router.Link;

var {List,TextField, RaisedButton,IconButton,FloatingActionButton,Paper} = mui;


var ListeGestionExercices=React.createClass({
  contextTypes: {
    router: React.PropTypes.func,
    notification:React.PropTypes.func.isRequired,
    muiTheme: React.PropTypes.object,

  },


  componentDidMount: function() {
    $.get("api/v1/resumeExercice/?format=json", function(result) {
      if (this.isMounted()) {
         this.setState({
           exercices: result["objects"],
            });
         }
      }.bind(this));
    },

  getInitialState: function() {
        return {
            exercices:[],
            recherche:"",
            };
        },

  getDefaultProps: function() {},
        componentWillMount : function() {},
        componentWillReceiveProps: function() {},
        componentWillUnmount : function() {},
        _parseData : function() {},
        _onSelect : function() {},



        handleChangeRecherche: function (event) {
            this.setState({
                recherche: event.target.value
            })
        },
  ajoutExercice: function(exercice)  {
    var exos=this.state.exercices;
    exos.push(exercice);
    this.setState({exercices:exos})
  },
  render: function() {
            var exercicesListe = this.state.exercices;
            var exercices=[];
            var recherche=this.state.recherche.toLowerCase();
            for (var i in exercicesListe) {
                var exercice=exercicesListe[i];
                var titre=exercice.titre
                var enonce=exercice.enonce
                var commentaires=exercice.commentaires
                var id=exercice.id
                if(titre.toLowerCase().indexOf(recherche)>-1 ||
                    enonce.toLowerCase().indexOf(recherche)>-1||
                     commentaires.toLowerCase().indexOf(recherche)>-1){
                    exercices.push(
                    <Exercice
                        color={exercice.dansProgression?"transparent":"seashell"}
                        key={titre+id}
                        titre={titre}
                        enonce={enonce}
                        id={id}
                        reussi={exercice.reussi}
                        commentaires={commentaires}
                        ajouterExo={this.ajoutExercice}
                    />
                    );
                }
            }
            var ajouter={}
            if(Auth.permit('enseignant'))
                ajouter=(
            <Paper
            zDepth="1"
            circle={true}
            style={{position: 'fixed',
                bottom: this.context.muiTheme.rawTheme.spacing.desktopGutter+'px',
                right: this.context.muiTheme.rawTheme.spacing.desktopGutter+'px',
                width:this.context.muiTheme.floatingActionButton.buttonSize+"px",
                height:this.context.muiTheme.floatingActionButton.buttonSize+"px",
                 textAlign: 'center',
                 verticalAlign: 'bottom',
                backgroundColor:this.context.muiTheme.floatingActionButton.color
            }}>
            <IconButton onClick={()=>this.context.router.transitionTo('creer_exercice')}
                tooltip="Nouvel exercice"
                tooltipPosition="top-center"
                style={{
                padding:"0",
                height:this.context.muiTheme.floatingActionButton.buttonSize+"px",

            }}>
            a
            <i
              style={{
                lineHeight: this.context.muiTheme.floatingActionButton.buttonSize + 'px',
             }}
            className="material-icons" >add</i>
            </IconButton>
            </Paper>

                );
            return (
                <div style={this.props.style}>
                    Les exercices disponibles.
                    <br/>
                   <TextField
                        hintText="Filtrer"
                        label='Filtrer dans le titre ou le sujet :'
                        onChange={this.handleChangeRecherche} />
                <Paper zDepth="1">
                   <List>
                        {exercices}
                   </List>
                </Paper>
                        {ajouter}
               </div>

                );
  },
});

module.exports=ListeGestionExercices
