var React = require('react');
var $ = require('jquery');

var Exercice = require('./Exercice.jsx');
var mui = require('material-ui');
var Auth=require('../policies/Auth.jsx')
var Router = require('react-router');
var marked = require('marked');

var {Paper, Card, CardText, List} = mui;

var Progression=React.createClass({
  contextTypes: {
    router: React.PropTypes.func
  },

  propTypes: {
                        titre: React.PropTypes.string,
                        sujet: React.PropTypes.string,
                        exercices: React.PropTypes.object,
                        id: React.PropTypes.number
  },

  getInitialState: function() {

        if(! this.props.sujet){
        return{titre:"",sujet:"",exercices:null}
        }
        else
        return {
            titre:this.props.titre,
            sujet:this.props.sujet,
            exercices:this.props.exercices
        };
  },
  getDefaultProps: function() {},
  componentWillMount : function() {},
  componentWillReceiveProps: function() {},
  componentWillUnmount : function() {},
  _parseData : function() {},
  _onSelect : function() {},

 componentDidMount: function() {

 $.get("api/v1/progression/"+this.props.params.id+"/?format=json",
            function(result)
                {
                    this.props.changeSousTitre(result.titre)
                    if (this.isMounted()) {
                        this.setState({
                        exercices: result.listeExercices,
                        sujet:result.sujet,
                        titre:result.titre
                        });
                    }

            }.bind(this));
  },

 render : function(){

            var exercices=[]
            for (var i in this.state.exercices) {
                var exercice=this.state.exercices[i]
                var titre=exercice.titre
                var enonce=exercice.enonce
                var id=exercice.id

                    exercices.push(
                    <Exercice
                        key={titre}
                        titre={titre}
                        enonce={enonce}
                        id={id}
                        reussi={exercice.reussi}
                        progression={this.props.params.id}
                    />
                    );
            }
        return (
        <div style={this.props.style}>
            <div style={{
              padding: '20px 0 1em 0',}}>
            <Card >
                    <CardText>
                        <div dangerouslySetInnerHTML={{__html: marked(this.state.sujet, {sanitize: true})}} />
                    </CardText>
            </Card>
            </div>
            <List>
                {exercices}
            </List>
        </div>
        )
        }
  })



module.exports = Progression;
