var $ = require('jquery');

var React = require('react');
var mui = require('material-ui');

var Auth=require('../policies/Auth.jsx')
var Router = require('react-router');
var Link=Router.Link;

var Progression =require("./Progression.jsx")
var EditeProgression =require("./EditeProgression.jsx")
var summarize = require('summarize-markdown');

var {List,ListItem, TextField, RaisedButton,IconButton,FloatingActionButton,Paper} = mui;

var Progressions=React.createClass({
  contextTypes: {
    router: React.PropTypes.func,
    notification:React.PropTypes.func.isRequired,
    muiTheme: React.PropTypes.object,

  },

  ajoutProgression:function(){

            $.ajax({
                type: 'POST',
                url: 'api/v1/progression/',
                data: '{"listeExercices":"[]"}',
                success:function(jqXHR, status)
                    {
			this.componentDidMount();
                    }.bind(this),
                error:(function(jqXHR, status, erreur)
                    {
                    }).bind(this),
            processData:  false,
            contentType: "application/json"
                });
  },



  componentDidMount: function() {
    $.get("api/v1/progression/?format=json", function(result) {
      if (this.isMounted()) {
         this.setState({
           progressions: result["objects"],
            });
         }
      }.bind(this));

       $.get("api/v1/exercice/?format=json", function(result) {
      if (this.isMounted()) {
         this.setState({
           tousExercices: result["objects"],
            });
         }
      }.bind(this));

    },

  getInitialState: function() {
        return {
            progressions:[],
            tousExercices:[],
            recherche:"",
            };
        },
        getDefaultProps: function() {},
        componentWillMount : function() {},
        componentWillReceiveProps: function() {},
        componentWillUnmount : function() {},
        _parseData : function() {},
        _onSelect : function() {},



        handleChangeRecherche: function (event) {
            this.setState({
                recherche: event.target.value
            })
        },
        render: function() {
            var progressionListe = this.state.progressions;
            var progressions=[];

            var recherche=this.state.recherche.toLowerCase();
            for (var i in progressionListe) {
                var progression=progressionListe[i];
                var sujet=progression.sujet
                var titre=progression.titre
                let idd=progression.id
                var exercices=progression.listeExercices
                if(titre.toLowerCase().indexOf(recherche)>-1 ||sujet.toLowerCase().indexOf(recherche)>-1 ){
                    progressions.push(
                    <ListItem
                        primaryText= {titre}
                        secondaryText={summarize(sujet)}
                        secondaryTextLines={2}
                        onClick={() => Auth.permit("enseignant")?  this.context.router.transitionTo('edite_progression', {id:idd}):
                        this.context.router.transitionTo('progression', {id:idd})
                        }
                     />

                    );
                }

            }
	    var boutonAjout=<div></div>
            if(Auth.permit("enseignant"))
	    boutonAjout=  <Paper
        	    zDepth="1"
            	    circle={true}
            style={{position: 'fixed',
                bottom: this.context.muiTheme.rawTheme.spacing.desktopGutter+'px',
                right: this.context.muiTheme.rawTheme.spacing.desktopGutter+'px',
                width:this.context.muiTheme.floatingActionButton.buttonSize+"px",
                height:this.context.muiTheme.floatingActionButton.buttonSize+"px",
                 textAlign: 'center',
                 verticalAlign: 'bottom',
                backgroundColor:this.context.muiTheme.floatingActionButton.color
            }}>
                <IconButton onClick={this.ajoutProgression.bind(this)}
                    tooltip="Nouveau sujet"
                    tooltipPosition="top-center"
                    style={{
                        padding:"0",
                        height:this.context.muiTheme.floatingActionButton.buttonSize+"px",
                    }}>
                 a
                <i style={{
                    lineHeight: this.context.muiTheme.floatingActionButton.buttonSize + 'px',
                    }}
                 className="material-icons" >add</i>
                </IconButton>
                </Paper>
		

            return (
                <div style={this.props.style}>
                   <TextField
                        key="filtre"
                        hintText="Filtrer"
                        label='Filtrer dans le titre ou le sujet :'
                        onChange={this.handleChangeRecherche} />
                    <List>
                        {progressions}
		
                   </List>

			{boutonAjout}

		</div>
)
  },
});

module.exports=Progressions
