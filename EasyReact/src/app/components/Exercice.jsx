var React = require('react');
var mui = require('material-ui');
var Auth=require('../policies/Auth.jsx')
var Router = require('react-router');
var marked = require('marked');
var summarize = require('summarize-markdown');
var {List, TextField, IconButton,ListItem, ToggleStarBorder, FontIcon} = mui;
var $ = require('jquery');




function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = $.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

$.ajaxSetup({
    beforeSend: function(xhr, settings) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
);


var Exercice=React.createClass({
  contextTypes: {
    router: React.PropTypes.func
  },

  propTypes: {
                        titre: React.PropTypes.string,
                        enonce: React.PropTypes.string,
                        reussi: React.PropTypes.bool,
                        progression: React.PropTypes.number,
                        ajouterExo: React.PropTypes.object,
                        commentaires:React.PropTypes.string
  },

  getInitialState: function() {
        return {
            enonce: this.props.enonce.substring(0,50),
            resume:true,
        };
  },
  getDefaultProps: function() {},
  componentWillMount : function() {},
  componentWillReceiveProps: function() {},
  componentWillUnmount : function() {},
  _parseData : function() {},
  _onSelect : function() {},

  render : function(){
        if(this.state.supprime)
            return(<div></div>)

        var outils;
        var res;
        if(Auth.permit('enseignant'))
            (
            outils=
                  <div>
                    <IconButton
                    tooltip="Essayer"
                    onClick={() => this.context.router.transitionTo('exercice', {id:this.props.id})}
                    >
                        <i className="material-icons" >fast_forward</i>
                    </IconButton>

                   <IconButton
                    tooltip="Editer"
                    onClick={() => this.context.router.transitionTo('gerer_exercice', {id:this.props.id})}
                    >
                        <i className="material-icons" >create</i>
                    </IconButton>

                    <IconButton
                    tooltip="Supprimer"
                    onClick={() => {
                                    if(confirm("Supprimer cet exerice ?"))
                                    { $.ajax({type: 'DELETE',
                                              url: "api/v1/gestion_exercice/"+this.props.id+"/",
                                              success:(function(jqXHR, status){
                                                    this.setState({supprime:true})
                                                    }).bind(this),
                                              error:(function(jqXHR, status, erreur){}).bind(this),
                                              })
                                    }
                                   }
                    }>
                        <i className="material-icons" >clear</i>
                    </IconButton>

                    <IconButton
                    tooltip="Clone"
                    onClick={() => {$.ajax({
                                    type: 'POST',
                                    url: "api/dupliquer/"+this.props.id+"",
                                    success:(function(jqXHR, status){
                                         this.props.ajouterExo({auteur:"",commentaires:this.props.commentaires,enonce:this.props.enonce, id:jqXHR.id, titre:jqXHR.titre})
                                    }).bind(this),
                                    error:(function(jqXHR, status, erreur){}).bind(this),
                                    })
                            }}
                    >
                        <i className="material-icons" >add circle</i>
                    </IconButton>
                  </div>
            )
        var reussi=this.props.reussi?(<i className="material-icons" >star</i>):(<div></div>);
        if(Auth.permit('enseignant'))
            res=    (<ListItem
                        primaryText=<div>{this.props.titre}{reussi}</div>
                        secondaryText={summarize(this.props.enonce)}
                        secondaryTextLines={2}
                        disabled={true}
                        rightIconButton={ outils }
			            style={{paddingRight:168, backgroundColor:this.props.color}}
                     >
                     </ListItem>)
        else
            res=    (<ListItem
                        style={this.props.style}
                        primaryText={this.props.titre}
                        secondaryText={summarize(this.props.enonce)}
                        secondaryTextLines={2}
                        rightIconButton={reussi}
                        onClick={() => this.context.router.transitionTo('exerciceProgression', {id:this.props.id,
                                progression:this.props.progression?this.props.progression:0})}
                     />
                        )

        return (res)

        }
  })



module.exports = Exercice;
