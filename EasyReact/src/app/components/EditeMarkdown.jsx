var React = require('react');
var mui = require('material-ui');
var {List,ListItem,TextField, RaisedButton} = mui;
var marked = require('marked');


var EditeMarkdown=React.createClass({
  contextTypes: {

  },
  getInitialState: function() {
            var val=(typeof this.props.value =="undefined") ? "" : this.props.value
            return {
              value:val,
           }
        },

        getDefaultProps: function() {},
        componentWillMount : function() {},
        componentWillReceiveProps: function() {},
        componentWillUnmount : function() {},
        _parseData : function() {},
        _onSelect : function() {},
        handleChange: function(event) {
            this.setState({value: event.target.value});
        },

        render: function() {
                 var rawMarkup = marked(this.state.value, {sanitize: true})
                var value = this.state.value;

                 return  (
                <div style={this.props.style}>
                    <div
                        id="ContenuPrincipal"
                        style={{
                            display:'flex',
                            flexWrap: 'wrap',
                     }}>
                          <TextField
                            floatingLabelText="Énoncé (markdown)"
                            hintText="Énoncé"
                            value={value}
                            multiLine={true}
                            fullWidth={true}
                            onChange={this.handleChange}
                            style={{
                                flex:'1',
                                minWidth:'240'
                            }}
                        />

                            <div
                                style={{
                                    flex:'1',
                                    minWidth:'240'
                                    }}>
                                <b> Énoncé mis en forme </b>
                                <div dangerouslySetInnerHTML={{__html: rawMarkup}} />
                            </div>
                     </div>

                </div>
                );

        }
});

module.exports=EditeMarkdown
