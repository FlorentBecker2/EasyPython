var $ = require('jquery');
 var Auth = (function(User, undefined) {


  var _policies = {
    enseignant: function() {
     try{
        if(localStorage.getItem("user")!=null)
        {
        var currentUser = JSON.parse(localStorage.getItem("user"))
        return ($.inArray("ens", currentUser.groups) != -1);
        }
        else
            return false
      }
    catch(err) {"Erreur Auth:"+console.log(err); return false}
    },
  };

  var permit = function(action, record) {
    return _policies[action](record);
  }

  return {
    permit: permit
  };

})()


module.exports=Auth
